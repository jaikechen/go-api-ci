#!/bin/bash
go run main.go &
sleep 1
pushd ./api-test || exit 1
npm install
node index.js
popd || exit 1
