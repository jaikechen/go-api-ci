package services

import "testing"

func TestHello(t *testing.T) {
	if !Hello() {
		t.Fatal("hello didn't return true")
	}
}
