package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

type Response struct {
	Title string `json:"title"`
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	response := Response{
		Title: "hello",
	}

	// Marshal the Response struct into JSON
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// Set the response header
	w.Header().Set("Content-Type", "application/json")

	// Write the JSON response to the response body
	w.Write(jsonResponse)
}

func main() {
	// Define the route and handler for the "/hello" endpoint
	http.HandleFunc("/", helloHandler)
	http.HandleFunc("/q", func(writer http.ResponseWriter, request *http.Request) {
		os.Exit(0)
	})

	// Start the HTTP server on port 8080
	fmt.Println("Server listening on port 8080...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
